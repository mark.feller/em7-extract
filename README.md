# Science Logic Bulk Data Extractor

A configurable bulk data extractor for science logic.

## Configuration

Configuration can be provided through command line flags, environment viarables, json, yaml, or toml files, and etcd in the listed order of presidence.

## Deamon

The application can be run as a deamon and accept GRPC calls to begin extraction.

