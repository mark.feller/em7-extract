package extractor

import (
	"io/ioutil"
	"log"
)

var (
	debug Logger
	info  Logger
)

// Logger interface used by package
type Logger interface {
	Printf(format string, v ...interface{})
	Println(v ...interface{})
	Fatalf(format string, v ...interface{})
}

func init() {
	debug = log.New(ioutil.Discard, "", log.LstdFlags)
	info = log.New(ioutil.Discard, "", log.LstdFlags)
}

// SetDebugLogger initializes debug logging
func SetDebugLogger(l Logger) {
	debug = l
}

// SetLogger initializes logger
func SetLogger(l Logger) {
	info = l
}
