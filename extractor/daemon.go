package extractor

import (
	"fmt"
	"net"
	"strings"

	pb "gitlab.com/t4spartners/em7-extract/protobuf"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var version string

// server is used to implement helloworld.GreeterServer.
type server struct {
	e *Em7Extractor
	l *MongoLoader
}

// Start implements ExtractServer
func (s *server) Start(ctx context.Context, in *pb.Em7ExtractRequest) (*pb.Em7ExtractResponse, error) {
	info.Println("handling request")
	ec := *s.e

	if in.User != "" {
		ec.User = in.User
	}
	if in.Passwd != "" {
		ec.Pass = in.Passwd
	}
	if in.Base != "" {
		ec.URL = in.Base
	}
	if len(in.Paths) != 0 {
		ec.Paths, _ = getPaths(in.Paths)
	}
	if in.Test {
		ec.test = true
	}
	if in.PoolSize != 0 {
		ec.PoolSize = int(in.PoolSize)
	}

	Cleanup(s.l.URI, s.l.DB, ec.Paths)
	ec.Extract()
	return &pb.Em7ExtractResponse{Error: ""}, nil
}

// Listen sets up a gRPC server and listens on 50051
func Listen(e *Em7Extractor, l *MongoLoader, port string) {
	lis, err := net.Listen("tcp", ":"+port)
	if err != nil {
		info.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterEm7ExtractServer(s, &server{e: e, l: l})
	// Register reflection service on gRPC server.
	reflection.Register(s)
	info.Printf("listening (%v)", port)
	if err := s.Serve(lis); err != nil {
		info.Fatalf("failed to serve: %v", err)
	}
}

func getPaths(pathStr []string) (map[string]interface{}, error) {
	if len(pathStr) == 0 {
		return nil, fmt.Errorf("path_str nil. No paths to export")
	}

	objects := make(map[string]interface{})
	for _, o := range pathStr {
		path := strings.Split(o, ".")
		add(objects, path)
	}
	return objects, nil
}

func add(paths map[string]interface{}, path []string) {
	if len(path) == 0 {
		return
	}

	next, ok := paths[path[0]]
	if !ok {
		paths[path[0]] = make(map[string]interface{})
		next = paths[path[0]]
	}
	add(next.(map[string]interface{}), path[1:])
}
