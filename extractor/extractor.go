package extractor

import (
	"crypto/tls"
	"net/http"
	"sync"
	"time"

	"github.com/ivpusic/grpool"
)

const (
	// how many records per page. used when getting nested data
	pageSize  = 500
	queueSize = 50

	// how many retry attempts when calling EM7 API
	retry = 3

	// default timeout on em7 api requests
	timeout = 10 * time.Second
)

// Em7Extractor imlpements the Extractor interface for grabbing data out of
// science logic
type Em7Extractor struct {
	User     string
	Pass     string
	URL      string
	Paths    map[string]interface{}
	PoolSize int // how many concurrent requests can be made

	count  int
	target Loader
	test   bool
	pool   *grpool.Pool
	client *http.Client
}

// WithLoader assigns loader for a given extractor
func (e *Em7Extractor) WithLoader(l Loader) *Em7Extractor {
	e.target = l
	return e
}

// Extract begins extraction proccess for science logic
func (e *Em7Extractor) Extract() {
	info.Println("Extraction beginning")
	if e.target == nil {
		info.Println("no targets")
		return
	}

	if e.PoolSize <= 0 {
		e.PoolSize = 100
	}
	info.Println("Request pool size:", e.PoolSize)

	e.count = 0
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	e.client = &http.Client{Timeout: timeout, Transport: tr}
	e.pool = grpool.NewPool(e.PoolSize, queueSize)
	e.target.Load(e.fanout())
	info.Println("requests made:", e.count)
	info.Println("Extraction done")
}

func (e *Em7Extractor) fanout() <-chan Object {
	out := make(chan Object)

	go func() {
		wg := &sync.WaitGroup{}

		for resource, paths := range e.Paths {
			p, ok := paths.(map[string]interface{})
			if ok {
				wg.Add(1)
				go e.extract(resource, p, out, wg)
			} else {
				info.Printf("ignoring unexpected resource paths: %#v", paths)
			}
		}

		wg.Wait()
		close(out)
	}()

	return out
}

func (e *Em7Extractor) extract(name string, paths map[string]interface{}, out chan Object, wg *sync.WaitGroup) {
	defer wg.Done()
	re := &ResourceExtractor{name, paths, e}
	re.Extract(out)
	info.Println("finished extracting", name)
}

// get requests an http path from science logic with retry logic and thread
// pooling to throttle load on server.
func get(e *Em7Extractor, path string, retry int) (chan *http.Response, chan error) {
	e.count++

	out := make(chan *http.Response, 1)
	errc := make(chan error, 1)

	e.pool.JobQueue <- func() {
		defer close(out)
		defer close(errc)

		oc, ec := getretry(e, path, retry)
		out <- oc
		errc <- ec
	}
	return out, errc
}

func getretry(e *Em7Extractor, path string, retry int) (*http.Response, error) {
	req, err := http.NewRequest("GET", e.URL+path, nil)
	if err != nil {
		return nil, err
	}

	req.SetBasicAuth(e.User, e.Pass)
	resp, err := e.client.Do(req)
	if err != nil {
		if retry > 0 {
			info.Printf("retying, retries left %d: %s", retry, path)
			return getretry(e, path, retry-1)
		} else {
			return nil, err
		}
	}
	return resp, nil
}
